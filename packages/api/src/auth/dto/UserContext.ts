import { MaybePromise } from "../AuthModuleOptions";

/**
 * The payload added to `request.user`
 */
export class UserContext<UserType> {

  id: string | number;

  roles: any[];

  /**
   * The JWT payload attached to this request.
   */
  jwt: any;

  /**
   * The entire user object - fetched if the {@link ParseUser} decorator set.
   */
  user?: UserType;

  getUser: () => MaybePromise<UserType>;

}
