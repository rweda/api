import { Injectable } from "@nestjs/common";
import { NanoIDService } from "@rweda/api";

@Injectable()
export class IDService {

  public constructor(
    private readonly id: NanoIDService,
  ) {}

  public user() {
    return this.id.safe("MILLION");
  }

}
