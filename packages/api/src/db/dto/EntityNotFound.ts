import { DataCategory, TaggedType } from "../../data";
import { FindManyOptions, FindOneOptions } from "typeorm";
import { NotFound } from "../../exceptions";

export class EntityNotFound<EntityName extends string = string> extends NotFound implements TaggedType {
  public _category: DataCategory.EXCEPTION;
  public _tag: "EntityNotFound";

  public readonly entityName: EntityName;

  public readonly search?: FindOneOptions | FindManyOptions;

  public constructor(entityName: EntityName, search?: FindOneOptions | FindManyOptions) {
    super();
    this._category = DataCategory.EXCEPTION;
    this._tag = "EntityNotFound";
    this.entityName = entityName;
    this.search = search;
  }

}
