import { ApiProperty } from "@nestjs/swagger";

export class WhoAmI {

  @ApiProperty()
  defined: boolean;

  @ApiProperty()
  id: string;
}
