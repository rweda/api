import { Module } from "@nestjs/common";
import { HashService } from "./HashService";

@Module({
  providers: [
    HashService,
  ],
  exports: [
    HashService,
  ],
})
export class HashModule {}
