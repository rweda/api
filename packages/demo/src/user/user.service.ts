import { Injectable, Logger } from "@nestjs/common";
import { InjectRepository } from "@nestjs/typeorm";
import { EntityService, FailedLoginException, HashService, InvalidPassword, isTaggedData } from "@rweda/api";
import {} from "bcrypt";
import { IDService } from "../id/id.service";
import { Repository } from "typeorm";
import { Role } from "./enum/Role";
import { User } from "./user.entity";

@Injectable()
export class UserService extends EntityService<User, "User"> {
  private readonly logger = new Logger(UserService.name);
  private readonly firstAdmin: Promise<User>;

  public constructor(
    private readonly id: IDService,
    private readonly hash: HashService,
    @InjectRepository(User)
    private readonly users: Repository<User>,
  ) {
    super("User", users);
    this.firstAdmin = this.ensureFirstAdmin();
  }

  /**
   * Authenticate a user.  Rejects with {@link FailedLoginException} if authentication fails,
   * otherwise resolves with the user that has logged in.
   */
  public async login(username: string, password: string): Promise<User> {
    const user = await this.findById(username);
    const hashedPassword = isTaggedData(user) ? user.data.password : undefined;
    const match = await this.hash.compare(password, hashedPassword ?? "invalid-password");
    if(!isTaggedData(user) || !match) {
      const reason = isTaggedData(user) ? new InvalidPassword() : user;
      throw new FailedLoginException(reason);
    }
    return user.data;
  }

  /**
   * Basic method that waits to resolve until a first admin has been created
   * (or verified to exist if one already has been created)
   */
  public async firstAdminCreated(): Promise<void> {
    await this.firstAdmin;
  }

  protected async ensureFirstAdmin(): Promise<User> {
    const admins = await this.find({
      where: {
        admin: true,
      },
      order: {
        createdAt: "ASC",
      },
    }, true);
    if(admins.length === 0) {
      this.logger.log("No admins - creating default user");
      const user = await this.createUser("admin", "admin", "Admin", [], true);
      this.logger.log("Created 'admin'");
      return user;
    } else {
      this.logger.verbose("Admin already exists.");
      return admins[0];
    }
  }

  public async createUser(
    username: string,
    password: string,
    name?: string,
    roles: Role[] = [],
    isAdmin?: boolean,
  ): Promise<User> {
    const user = new User();
    user.uuid = await this.id.user();
    user.id = username;
    user.password = await this.hash.hash(password);
    user.name = name;
    user.admin = isAdmin === true;
    user.roles = roles;
    return this.users.save(user);
  }

}
