import { HttpStatus } from "@nestjs/common";
import { ApiException } from "./ApiException";

/**
 * A base class generally used for 3rd-party or unexpected exceptions.
 * Often the details can be hidden from users, and a generic "something wrong happened" is shown.
 */
export class InternalException extends ApiException<HttpStatus.INTERNAL_SERVER_ERROR> {

  public readonly statusCode: HttpStatus.INTERNAL_SERVER_ERROR;

  public constructor() {
    super(HttpStatus.INTERNAL_SERVER_ERROR);
  }

}
