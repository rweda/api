import { ApiProperty } from "@nestjs/swagger";
import { User } from "../user.entity";
import { UserSummary } from "./UserSummary";

export class LoginSuccess {

  public static async format(user: User, jwt: string): Promise<LoginSuccess> {
    return {
      user: await UserSummary.format(user),
      jwt,
    };
  }

  @ApiProperty()
  user: UserSummary;

  @ApiProperty()
  jwt: string;

}
