import { Module } from '@nestjs/common';
import { TypeOrmModule } from "@nestjs/typeorm";
import { AuthModule, HashModule, NanoIDModule } from '@rweda/api';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { IDModule } from './id/id.module';
import { JwtPayload } from './user/dto/JwtPayload';
import { User } from './user/user.entity';
import { UserModule } from './user/user.module';
import { UserService } from './user/user.service';

@Module({
  imports: [
    TypeOrmModule.forRoot({
      type: "sqlite",
      database: "test.sql",
      entities: [
        User,
      ],
      synchronize: true,
    }),
    AuthModule.forRootAsync({
      global: true,
      imports: [UserModule],
      inject: [UserService],
      useFactory: (users: UserService) => ({
        secretOrPublicKey: "test",
        jwtOptions: {
          issuer: "demo",
        },
        defaultPolicy: "allow",
        payload: JwtPayload,
        userId: ({ sub }) => sub,
        getUserById: id => users.findById(id, true),
        getRoles: () => [],
      }),
    }),
    NanoIDModule,
    HashModule,
    IDModule,
    UserModule,
  ],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule {}
