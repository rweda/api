= Publish OpenAPI Spec

Many components of the API Utilities ship objects annotated with OpenAPI specifications,
allowing you to automatically generate API docs and client libraries.

In your `main.ts` file where you create a Nest Application, add configuration to publish
an OpenAPI specification:

.`main.ts`
[source,ts]
----
import { writeFile } from "fs/promises";
import { join } from "path";
import { DocumentBuilder, SwaggerModule } from "@nestjs/swagger";
// [... existing imports]

// existing function:
async function bootstrap() {
  const app = await NestFactory.create(AppModule);

  // after `app` is defined, add the following:
  const apiSpecConfig = new DocumentBuilder()
    .setTitle("Demo Server")
    .setVersion("1.0")
    .build();
  const apiSpec = SwaggerModule.createDocument(app, apiSpecConfig, {
    operationIdFactory: (k,m) => m,
  });
  // write API spec to a file:
  await writeFile(join(__dirname, "../dist/demo.json"), JSON.stringify(apiSpec, null, 2));
  // publish docs under an /api/ route:
  SwaggerModule.setup("api", app, document);

  if(process.env.CODEGEN_ONLY) {
    process.exit(0);
  }

  // [... remaining code]
}
----

You may wish to omit some of the actions preformed above:

- Remove `writeFile(...)` if you don't want to write the specification to disk
- Remove `SwaggerModule.setup(...)` if you don't want to publish API documentation
- Remove `if(process.env.CODEGEN_ONLY) { ... }` if you don't need to generate API docs in scripts
  and want the server to automatically stop.
