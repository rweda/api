import { ArgumentsHost, Catch, ExceptionFilter } from "@nestjs/common";
import { instanceToPlain } from "class-transformer";
import type { Request, Response } from "express";
import { ApiException } from "./ApiException";

/**
 * A NestJS exception filter that formats any thrown {@link ApiException} -
 * uses `class-validator` and `class-transformer` to expose properties
 * on the error marked with `@Expose`.
 */
@Catch(ApiException)
export class ApiExceptionFilter implements ExceptionFilter {

  catch(exception: ApiException, host: ArgumentsHost) {
    const ctx = host.switchToHttp();
    const response = ctx.getResponse<Response>();
    const request = ctx.getRequest<Request>();
    const status = exception.getStatus();

    const formatted = instanceToPlain(exception, {
      strategy: "excludeAll",
    });

    response.status(status).json(formatted);
  }

}
