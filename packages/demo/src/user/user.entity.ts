import { Column, CreateDateColumn, Entity, PrimaryColumn } from "typeorm";
import { Role } from "./enum/Role";

@Entity()
export class User {

  /**
   * A randomly assigned ID.
   * (mostly just an example of the ID service)
   */
  @Column()
  uuid: string;

  /**
   * A unique username.
   */
  @PrimaryColumn()
  id: string;

  /**
   * The user's hashed password.
   */
  @Column({
    nullable: true,
  })
  password?: string;

  @Column({
    nullable: true,
  })
  name?: string;

  @Column({
    default: false,
  })
  admin: boolean;

  @Column("simple-array")
  roles: Role[];

  @CreateDateColumn()
  createdAt: Date;

}
