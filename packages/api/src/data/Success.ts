import { DataCategory } from "./DataCategory";
import { TaggedType } from "./TaggedType";

/**
 * Generic type that can represent a successful value returned by a method,
 * wrapped inside a {@link TaggedType} for differentiation.
 */
export class Success<T> implements TaggedType {
  public readonly _category: DataCategory.DATA;
  public readonly _tag: "Success";

  public readonly data: T;

  public constructor(data: T) {
    this._category = DataCategory.DATA;
    this._tag = "Success";
    this.data = data;
  }

}

export function isSuccess<T>(data: TaggedType): data is Success<T> {
  return data._tag === "Success";
}
