import { Test, TestingModule } from '@nestjs/testing';
import { HttpStatus, INestApplication } from '@nestjs/common';
import { AppModule } from '../src/app.module';
import { makeFetch } from "supertest-fetch";
import { BaseHttpRequest, CancelablePromise, DemoClient, OpenAPIConfig } from './helpers/generated';
import { ApiRequestOptions } from './helpers/generated/core/ApiRequestOptions';
import { request } from './helpers/override/core/request';
import { Either, isLeft, isRight, Left, Right } from 'fp-ts/lib/Either';
import { ApiExceptionFilter } from '@rweda/api';

function assertLeft<X,Y>(x: Either<X,Y>): asserts x is Left<X> {
  if(!isLeft(x)) {
    throw new Error(`Expected to be Left: ${JSON.stringify(x)}`);
  }
}

function assertRight<X,Y>(x: Either<X,Y>): asserts x is Right<Y> {
  if(!isRight(x)) {
    throw new Error(`Expected to be Right: ${JSON.stringify(x)}`);
  }
}

describe('App (e2e)', () => {
  let app: INestApplication;
  let unauthenticatedClient: DemoClient;
  let client: DemoClient;

  beforeEach(async () => {
    const moduleFixture: TestingModule = await Test.createTestingModule({
      imports: [AppModule],
    }).compile();

    app = moduleFixture.createNestApplication();
    app.useGlobalFilters(new ApiExceptionFilter());
    await app.init();

    const server = app.getHttpServer();
    const fetch = makeFetch(server);

    class MockHttpRequest extends BaseHttpRequest {
      constructor(config: OpenAPIConfig) {
        super(config);
      }

      public override request<T>(options: ApiRequestOptions): CancelablePromise<T> {
        return request(this.config, options, fetch as any);
      }
    }

    unauthenticatedClient = new DemoClient({}, MockHttpRequest);

    await unauthenticatedClient.users.hasFirstAdmin();
    const admin = await unauthenticatedClient.auth.login({ username: "admin", password: "admin" });

    client = new DemoClient({
      HEADERS: {
        Authorization: `Bearer ${admin.jwt}`,
      },
    }, MockHttpRequest);
  });

  it('/ (GET)', async () => {
    const res = await unauthenticatedClient.default.getHello();
    expect(res).toBe("Hello World!");
  });

  describe("auth", () => {

    it("can login as 'admin' by default", async () => {
      const res = await unauthenticatedClient.auth.login({
        username: "admin",
        password: "admin",
      });
      expect(res.user.username).toBe("admin");
      expect(typeof res.jwt).toBe("string");
    });

    it("blocks invalid credentials", async () => {
      const anyResponse = await unauthenticatedClient.auth.loginEither({
        username: "unknown-user",
        password: "password",
      });
      assertLeft(anyResponse);
      const res = anyResponse.left;
      expect(res.status).toBe(401);
      expect(res.body.message).toBe("Failed Login");
    });

    describe("@CurrentUserId", () => {

      it("can get the current user", async () => {
        const res = await client.users.whoami();
        expect(res.defined).toBe(true);
      });

      it("can be used in an optional way", async () => {
        const res = await unauthenticatedClient.users.whoami();
        expect(res.defined).toBe(false);
      });

      it("can get users in strict mode", async () => {
        const res = await client.users.whoamiStrict();
        expect(res.defined).toBe(true);
      });

      it("throws errors if strict mode and no client", async () => {
        const res = await unauthenticatedClient.users.whoamiStrictEither();
        assertLeft(res);
        expect(res.left.status).toBe(HttpStatus.INTERNAL_SERVER_ERROR);
      });

      it("can be guarded", async () => {
        const res = await unauthenticatedClient.users.whoamiGuardedEither();
        assertLeft(res);
        expect(res.left.status).toBe(HttpStatus.FORBIDDEN);
      });

    });

    describe("@CurrentUser", () => {

      it("can fetch users", async () => {
        const res = await client.users.whoamiFull();
        expect(res.defined).toBe(true);
      });

      it("can be used in optional mode", async () => {
        const res = await unauthenticatedClient.users.whoamiFull();
        expect(res.defined).toBe(false);
      });

    });

  });
});
