import { DataCategory, TaggedType } from "../../data";
import { Unauthorized } from "../../exceptions";

export class InvalidPassword extends Unauthorized implements TaggedType {
  public readonly _category: DataCategory.EXCEPTION;
  public readonly _tag: "InvalidPassword";

  public constructor() {
    super();
    this._category = DataCategory.EXCEPTION;
    this._tag = "InvalidPassword";
  }
}
