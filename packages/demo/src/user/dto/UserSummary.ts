import { ApiProperty } from "@nestjs/swagger";
import { User } from "../user.entity";

export class UserSummary {

  public static async format(user: User): Promise<UserSummary> {
    return {
      username: user.id,
      createdAt: user.createdAt.toISOString(),
    };
  }

  @ApiProperty()
  username: string;

  @ApiProperty()
  createdAt: string;

}
