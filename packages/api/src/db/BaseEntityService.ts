import { FindManyOptions, FindOneOptions, ObjectLiteral, Repository } from "typeorm";
import { Success } from "../data";
import { DatabaseError, EntityNotFound } from "./dto";

/**
 * A base set of utilities designed around a TypeORM repository.
 *
 * The {@link BaseEntityService} provides completely entity-agnostic
 * utilities.
 *
 * For most entities, you should use {@link EntityService}, which adds
 * additional shortcuts, but has some required fields.
 */
export abstract class BaseEntityService<T extends ObjectLiteral, EntityName extends string = string> {

  /**
   * The name of the entity.
   * Traditionally the name of the entity class, with human-readable variants
   * generated with an internationalization library.
   */
  protected readonly entityName: EntityName;

  /**
   * The TypeORM {@link Repository} for the entities this service is based around.
   */
  protected readonly repo: Repository<T>;

  /**
   * Create a new service based around a TypeORM {@link Repository}.
   *
   * @param entityName The name of the entity.  Traditionally the class name.
   * @param repo The TypeORM {@link Repository} to wrap.
   */
  public constructor(
    entityName: EntityName,
    repo: Repository<T>,
  ) {
    this.entityName = entityName;
    this.repo = repo;
  }

  /**
   * Get every entity in the database.
   * Returns a {@link DatabaseError} if any errors are thrown.
   */
  public async listAll(): Promise<Success<T[]> | DatabaseError>;
  /**
   * Get every entity in the database, throwing a {@link DatabaseError} if any errors are thrown.
   */
  public async listAll(required: true): Promise<T[]>;
  public async listAll(required?: true): Promise<T[] | Success<T[]> | DatabaseError> {
    return this.find({}, required);
  }

  public async find(options: FindManyOptions<T>, required: true): Promise<T[]>;
  public async find(options: FindManyOptions<T>): Promise<Success<T[]> | DatabaseError>;
  public async find(
    options: FindManyOptions<T>,
    required?: true,
  ): Promise<T[] | Success<T[]> | DatabaseError> {
    try {
      const data = await this.repo.find(options);
      if(required) {
        return data;
      }
      return new Success(data);
    } catch (e) {
      if(required) {
        throw new DatabaseError(e);
      }
      return new DatabaseError(e);
    }
  }

  public async findOne(options: FindOneOptions<T>, required: true): Promise<T>;
  public async findOne(options: FindOneOptions<T>): Promise<Success<T> | EntityNotFound<EntityName> | DatabaseError>;
  public async findOne(
    options: FindOneOptions<T>,
    required?: true,
  ): Promise<T | Success<T> | EntityNotFound<EntityName> | DatabaseError> {
    try {
      const data = await this.repo.findOne(options);
      if(data) {
        return required ? data : new Success(data);
      } else if(required) {
        throw new EntityNotFound(this.entityName, options);
      }
      return new EntityNotFound(this.entityName, options);
    } catch (e) {
      if(required) {
        throw new DatabaseError(e);
      }
      return new DatabaseError(e);
    }
  }

}
