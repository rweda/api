import { DatabaseError, EntityNotFound } from "../../db";
import { DataCategory, TaggedType } from "../../data";
import { Unauthorized } from "../../exceptions";
import { InvalidPassword } from "./InvalidPassword";
import { ApiProperty } from "@nestjs/swagger";
import { IsString } from "class-validator";
import { Expose } from "class-transformer";

export type FailedLoginCoreReasons
  = EntityNotFound
  | DatabaseError
  | InvalidPassword;

export class FailedLoginException<
  Reason extends FailedLoginCoreReasons = FailedLoginCoreReasons,
> extends Unauthorized implements TaggedType {
  public readonly _category: DataCategory.EXCEPTION;
  public readonly _tag: "FailedLoginException";

  @ApiProperty()
  @IsString()
  @Expose()
  public readonly message: "Failed Login";

  public readonly reason: Readonly<Reason>;

  public constructor(reason: Reason) {
    super();
    this.message = "Failed Login";
    this.reason = Object.freeze(reason);
  }

}
