import { Controller, Get } from "@nestjs/common";
import { ApiOkResponse, ApiTags } from "@nestjs/swagger";
import { CurrentUser, CurrentUserId, ParseUser, Private } from "@rweda/api";
import { WhoAmI } from "./dto/WhoAmI";
import { User } from "./user.entity";
import { UserService } from "./user.service";

@Controller("user")
@ApiTags("users")
export class UserController {

  public constructor(
    private readonly users: UserService,
  ) {}

  @Get("has-admin")
  public async hasFirstAdmin() {
    await this.users.firstAdminCreated();
    return "OK";
  }

  @Get("whoami")
  @ApiOkResponse({ type: WhoAmI })
  public async whoami(
    @CurrentUserId(true) user?: string,
  ): Promise<WhoAmI> {
    return {
      defined: typeof user === "string",
      id: user,
    };
  }

  @Get("whoami-strict")
  @ApiOkResponse({ type: WhoAmI })
  public async whoamiStrict(
    @CurrentUserId() user: string,
  ): Promise<WhoAmI> {
    return {
      defined: typeof user === "string",
      id: user,
    };
  }

  @Get("whoami-guarded")
  @Private()
  @ApiOkResponse({ type: WhoAmI })
  public async whoamiGuarded(
    @CurrentUserId() user: string,
  ): Promise<WhoAmI> {
    return {
      defined: typeof user === "string",
      id: user,
    };
  }

  @Get("whoami-full")
  @ParseUser()
  @ApiOkResponse({ type: WhoAmI })
  public async whoamiFull(
    @CurrentUser(true) user?: User,
  ): Promise<WhoAmI> {
    return {
      defined: typeof user?.id === "string",
      id: user?.id,
    };
  }

}
