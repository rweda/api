import { Inject, Injectable } from "@nestjs/common";
import { AUTH_MODULE_OPTIONS } from "./auth.constants";
import { AuthModuleOptions } from "./AuthModuleOptions";

@Injectable()
export class AuthService<
  JWTPayload extends object = object,
  User = any,
> {

  public constructor(
    @Inject(AUTH_MODULE_OPTIONS)
    private readonly options: AuthModuleOptions<JWTPayload, User>,
  ) {}

}
