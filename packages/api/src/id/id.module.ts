import { Module } from "@nestjs/common";
import { NanoIDService } from "./id.service";

@Module({
  providers: [
    NanoIDService,
  ],
  exports: [
    NanoIDService,
  ],
})
export class NanoIDModule {}
