import { HttpException, HttpStatus } from "@nestjs/common";
import { DataCategory } from "../data";

/**
 * A base exception/data type that can be either returned as data, or thrown if there's no way
 * to handle the error.
 *
 * Integrates with the {@link ApiExceptionFilter} to properly format exceptions.
 */
export class ApiException<
  StatusCode extends HttpStatus = HttpStatus,
> extends HttpException {
  public readonly _category: DataCategory.EXCEPTION;

  public readonly statusCode: StatusCode;

  public constructor(statusCode: StatusCode) {
    super("API Exception", statusCode);
    this._category = DataCategory.EXCEPTION;
    this.statusCode = statusCode;
  }

}
