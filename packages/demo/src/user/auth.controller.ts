import { Body, Controller, Post } from "@nestjs/common";
import { ApiOkResponse, ApiTags, ApiUnauthorizedResponse } from "@nestjs/swagger";
import { JwtService } from "@nestjs/jwt";
import { FailedLoginException, ParseUser } from "@rweda/api";
import { LoginRequest } from "./dto/LoginRequest";
import { LoginSuccess } from "./dto/LoginResponse";
import { UserService } from "./user.service";
import { JwtPayload } from "./dto/JwtPayload";

@Controller("auth")
@ApiTags("auth")
export class AuthController {

  public constructor(
    private readonly jwt: JwtService,
    private readonly users: UserService,
  ) {}

  @Post("login")
  @ApiOkResponse({ type: LoginSuccess })
  @ApiUnauthorizedResponse({ type: FailedLoginException })
  public async login(
    @Body() auth: LoginRequest,
  ): Promise<LoginSuccess> {
    const user = await this.users.login(auth.username, auth.password);
    const payload: JwtPayload = {
      sub: user.id,
    };
    const jwt = this.jwt.sign(payload);
    return LoginSuccess.format(user, jwt);
  }

}
