import { HttpStatus } from "@nestjs/common";
import { ApiProperty } from "@nestjs/swagger";
import { Expose } from "class-transformer";
import { IsNumber } from "class-validator";
import { ApiException } from "./ApiException";

/**
 * Base class for all HTTP Unauthorized exceptions.
 */
export class Unauthorized extends ApiException<HttpStatus.UNAUTHORIZED> {

  @ApiProperty()
  @IsNumber()
  @Expose()
  public readonly statusCode: HttpStatus.UNAUTHORIZED;

  public constructor() {
    super(HttpStatus.UNAUTHORIZED);
  }

}
