import { DataCategory, TaggedType } from "../../data";
import { InternalException } from "../../exceptions";

export function isDatabaseError(data: TaggedType): data is DatabaseError {
  return data._tag === "DatabaseError";
}

export class DatabaseError extends InternalException implements TaggedType {
  public readonly _category: DataCategory.EXCEPTION;
  public readonly _tag: "DatabaseError";

  public readonly error: Error;

  public constructor(error: Error) {
    super();
    this._category = DataCategory.EXCEPTION;
    this._tag = "DatabaseError";
    this.error = error;
  }

}
