import { Success } from "../data";
import { Equal, FindOptionsWhere, FindOptionsWhereProperty, ObjectLiteral } from "typeorm";
import { BaseEntityService } from "./BaseEntityService";
import { DatabaseError, EntityNotFound } from "./dto";

export type CommonEntity = ObjectLiteral & {
  id: string | number;
};

export class EntityService<
  T extends CommonEntity,
  EntityName extends string = string,
> extends BaseEntityService<T, EntityName> {

  public async findById(id: T["id"], required: true): Promise<T>;
  public async findById(id: T["id"]): Promise<Success<T> | EntityNotFound<EntityName> | DatabaseError>;
  public async findById(
    idValue: T["id"],
    required?: true,
  ): Promise<T | Success<T> | EntityNotFound<EntityName> | DatabaseError> {
    const id = idValue as FindOptionsWhereProperty<NonNullable<T["id"]>>;
    const where = { id } as FindOptionsWhere<T>;
    return this.findOne({ where }, required);
  }

}
