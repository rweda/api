import { Secret, VerifyOptions } from "jsonwebtoken";
import { ModuleMetadata } from "@nestjs/common";
import type { ClassType } from "class-transformer-validator";

export type MaybePromise<T> = T | Promise<T>;

/**
 * Configuration for authentication parsing and validation.
 */
export interface AuthModuleOptions<
  JWTPayload extends object,
  User,
  UserId extends string | number = string | number,
> {

  /**
   * Authentication to use when decoding JWTs.
   */
  secretOrPublicKey: Secret;

  /**
   * Options to provide to `jwt.verify` - see documentation
   * for `jsonwebtoken`.
   */
  jwtOptions?: VerifyOptions;

  /**
   * Configure the default behavior of the request guard for routes
   * that do not set any authentication parameters.
   *
   * Defaults to `"allow"`, allowing unauthenticated users access.
   */
  defaultPolicy?: "allow" | "deny";

  /**
   * A response to send if a JWT has expired.
   */
  responseExpiredJWT?: string | object;

  /**
   * A response to send if an incorrect JWT has been sent -
   * may occur from multiple errors, including unsigned JWTs,
   * incorrect audiences, etc.
   *
   * By default, a {@link UnauthorizedException} is thrown,
   * with a brief explanation.
   */
  responseBadJWT?: string | object;

  /**
   * A response to send if a JWT is unparsable or otherwise invalidly sent.
   * Defaults to a default Nest {@link BadRequestException} with a short
   * description of the situation.
   */
  responseInvalidJWT?: string | object;

  /**
   * A response to send if a JWT doesn't match the validation set on
   * the {@link payload}.
   */
  responseInvalidPayload?: string | object;

  responseForbiddenPayload?: string | object;

  /**
   * The structure of JWT payloads to parse.
   * Should be a class annotated with `class-validator` annotations.
   */
  payload: ClassType<JWTPayload>;

  /**
   * A method to extract the user ID from a parsed JWT payload.
   * Usually should be `payload => payload.sub` for most JWT structures.
   */
  userId: (payload: JWTPayload) => UserId;

  /**
   * Get the roles that a user belongs to.
   */
  getRoles: (payload: JWTPayload, userId: UserId) => MaybePromise<any[]>;

  /**
   * A method to fetch a user's details given the user ID embedded in a JWT.
   */
  getUserById: (id: UserId) => MaybePromise<User>;

}

export interface AuthModuleAsyncOptions<
  JWTPayload extends object,
  User,
  UserId extends string | number = string | number,
> extends Pick<ModuleMetadata, "imports"> {

  /**
   * If set, makes the {@link AuthGuard} the global `APP_GUARD` for all routes.
   * Otherwise, {@link AuthGuard} will be exported as a regular service.
   */
  global?: boolean;

  inject?: any[];

  useFactory?: (...args: any[]) => MaybePromise<AuthModuleOptions<
    JWTPayload,
    User,
    UserId
  >>;
}
