export class JwtPayload {

  /**
   * The user's username
   */
  sub: string;

}
