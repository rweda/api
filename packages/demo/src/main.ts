import { NestFactory } from '@nestjs/core';
import { DocumentBuilder, SwaggerModule } from '@nestjs/swagger';
import { ApiExceptionFilter } from "@rweda/api";
import { writeFile } from 'fs/promises';
import { join } from 'path';
import { AppModule } from './app.module';

async function bootstrap() {
  const app = await NestFactory.create(AppModule);
  app.useGlobalFilters(new ApiExceptionFilter());

  const config = new DocumentBuilder()
    .setTitle("Demo Server")
    .setVersion("1.0")
    .build();
  const document = SwaggerModule.createDocument(app, config, {
    operationIdFactory: (k,m) => m,
  });
  await writeFile(join(__dirname, "../dist/demo.json"), JSON.stringify(document, null, 2));
  SwaggerModule.setup("api", app, document);

  if(process.env.CODEGEN_ONLY) {
    process.exit(0);
  }

  await app.listen(3000);
}
bootstrap();
