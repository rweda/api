import { DataCategory } from "./DataCategory";

export interface TaggedType {
  _category: DataCategory;
  _tag: string;
}

export interface TaggedData extends TaggedType {
  _category: DataCategory.DATA;
}

export function isTaggedData(val: TaggedType): val is TaggedData {
  return val._category === DataCategory.DATA;
}
