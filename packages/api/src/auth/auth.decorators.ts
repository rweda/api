import { createParamDecorator, ExecutionContext, InternalServerErrorException, SetMetadata } from "@nestjs/common";
import type { Request } from "express";
import { AUTH_FETCH_COMPLETE_USER, AUTH_ROUTE_PUBLIC, AUTH_ROUTE_REQUIRE_ONE_OF_ROLES, AUTH_ROUTE_REQUIRE_ROLES } from "./auth.constants";
import { UserContext } from "./dto/UserContext";

export const Public = () => SetMetadata(AUTH_ROUTE_PUBLIC, true);

export const Private = () => SetMetadata(AUTH_ROUTE_PUBLIC, false);

export const ParseUser = () => SetMetadata(AUTH_FETCH_COMPLETE_USER, true);

export const RequireRoles = (...roles: any[]) => SetMetadata(AUTH_ROUTE_REQUIRE_ROLES, roles);

export const RequireOneOfRole = (...roles: any[]) => SetMetadata(AUTH_ROUTE_REQUIRE_ONE_OF_ROLES, roles);

export const CurrentUserId = createParamDecorator<boolean | undefined>(
  (optional, context: ExecutionContext) => {
    const req = context.switchToHttp().getRequest<Request>();
    const session: undefined | UserContext<any> = (req as any).session;

    if(optional === true) {
      return session?.id;
    }

    if(!session) {
      throw new InternalServerErrorException();
    }

    return session.id;
  },
);

/**
 * Returns a `Promise` that contains the current user.
 */
export const CurrentUserFetch = createParamDecorator<boolean | undefined>(
  (optional, context: ExecutionContext) => {
    const req = context.switchToHttp().getRequest<Request>();
    const session: undefined | UserContext<any> = (req as any).session;

    if(optional !== true && !session) {
      throw new InternalServerErrorException();
    }

    return session.getUser();
  },
);

/**
 * Retrieve the complete user identity associated with the current request.
 *
 * You must have added the {@link ParseUser()} decorator to the method or class
 * before using this method, otherwise the user won't be parsed in time.
 *
 * (You may alternatively use the {@link CurrentUserFetch}) parameter
 * which doesn't require parsing the user)
 */
export const CurrentUser = createParamDecorator<boolean | undefined>(
  (optional, context: ExecutionContext) => {
    const req = context.switchToHttp().getRequest<Request>();
    const session: undefined | UserContext<any> = (req as any).session;

    if(optional !== true && (!session || !session.user)) {
      throw new InternalServerErrorException();
    }

    return session?.user;
  },
);

export const CurrentJWT = createParamDecorator<boolean | undefined>(
  (optional, context: ExecutionContext) => {
    const req = context.switchToHttp().getRequest<Request>();
    const session: undefined | UserContext<any> = (req as any).session;

    if(optional !== true && (!session || !session.jwt)) {
      throw new InternalServerErrorException();
    }

    return session?.jwt;
  }
)
