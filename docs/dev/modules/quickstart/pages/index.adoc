= Getting Started with the API Utilities

You will want to install the API utilities and peer dependencies in your application:

== Configure GitLab Registry

To install directly from GitLab you will need to configure NPM/Yarn.

For NPM or Yarn 1, run:

.NPM and Yarn 1
[source,sh]
----
npm config set @rweda:registry https://gitlab.com/api/v4/packages/npm/
----

For Yarn 2+, edit `.yarnrc.yml`:

.`.yarnrc.yml` (Yarn 2+)
[source,yml]
----
npmScopes:
  rweda:
    npmRegistryServer: "https://gitlab.com/api/v4/packages/npm/"
----

== Install via Yarn

[source,sh]
----
yarn add \
  @rweda/api \
  @nestjs/common \
  @nestjs/core \
  @nestjs/swagger \
  typeorm
----
