import { Injectable } from "@nestjs/common";
import { customAlphabet } from "nanoid/async";
import { alphanumeric, nolookalikesSafe } from "nanoid-dictionary";

export const MAX_NOLOOKALIKES_SIZE = {
  HUNDRED: 4,
  THOUSAND: 5,
  THOUSANDS: 6,
  TENS_OF_THOUSANDS: 7,
  HUNDRED_THOUSAND: 8,
  MILLION: 9,
  MILLIONS: 10,
  FIFTY_MILLION: 11,
  HUNDRED_MILLION: 12,
  BILLION: 13,
} as const;

export const MAX_ALPHANUMERIC_SIZE = {
  HUNDREDS: 4,
  THOUSANDS: 5,
  TENS_OF_THOUSANDS: 6,
  HUNDRED_THOUSANDS: 7,
  MILLIONS: 8,
  TEN_MILLION: 9,
  HUNDRED_MILLION: 10,
  BILLION: 11,
} as const;

@Injectable()
export class NanoIDService {

  private readonly safeGenerator: (size?: number) => Promise<string>;
  private readonly alphanumericGenerator: (size?: number) => Promise<string>;

  public constructor() {
    this.safeGenerator = customAlphabet(nolookalikesSafe);
    this.alphanumericGenerator = customAlphabet(alphanumeric);
  }

  public async safe(size: keyof typeof MAX_NOLOOKALIKES_SIZE) {
    const length = MAX_NOLOOKALIKES_SIZE[size];
    return this.safeGenerator(length);
  }

  public async alphanumeric(size: keyof typeof MAX_ALPHANUMERIC_SIZE) {
    const length = MAX_ALPHANUMERIC_SIZE[size];
    return this.alphanumericGenerator(length);
  }

}
