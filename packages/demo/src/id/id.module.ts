import { Module } from "@nestjs/common";
import { NanoIDModule } from "@rweda/api";
import { IDService } from "./id.service";

@Module({
  imports: [
    NanoIDModule,
  ],
  providers: [
    IDService,
  ],
  exports: [
    IDService,
  ],
})
export class IDModule {}
