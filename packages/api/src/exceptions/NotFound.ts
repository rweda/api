import { HttpStatus } from "@nestjs/common";
import { ApiException } from "./ApiException";

export class NotFound extends ApiException<HttpStatus.NOT_FOUND> {

  public readonly statusCode: HttpStatus.NOT_FOUND;

  public constructor() {
    super(HttpStatus.NOT_FOUND);
  }

}
