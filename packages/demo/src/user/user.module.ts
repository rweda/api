import { Module } from "@nestjs/common";
import { TypeOrmModule } from "@nestjs/typeorm";
import { JwtModule } from "@nestjs/jwt";
import { HashModule } from "@rweda/api";
import { AuthController } from "./auth.controller";
import { UserController } from "./user.controller";
import { User } from "./user.entity";
import { UserService } from "./user.service";
import { IDModule } from "../id/id.module";

@Module({
  imports: [
    TypeOrmModule.forFeature([
      User,
    ]),
    JwtModule.register({
      secret: "test",
      signOptions: {
        issuer: "demo",
      },
    }),
    HashModule,
    IDModule,
  ],
  providers: [
    UserService,
  ],
  controllers: [
    AuthController,
    UserController,
  ],
  exports: [
    UserService,
  ],
})
export class UserModule {}
